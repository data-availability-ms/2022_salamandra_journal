#### 2. Bias estimation for niche breadth and niche overlap measures#### 

#### Content: #####

# 2.1 1D (~Shannon exponential measure) and 2D (~Levins measure) Bias estmation
# 2.2 Renkonen (or overlap percentage), Morisita and Pianka symmetric index bias estimation
# 2.3 1Db and 2Db bias estimation
# 2.4 Figures 3 and 4
# 2.5 Figures 5 and 6

##Expand or contract sections to display or hide the code ##

##### 2.1 1D (~Shannon exponential measure) and 2D (~Levins measure) Bias estimation ##########

## Packages

library(dplyr)

## data

m1 <- read.csv("width05.csv", header = T, row.names = 1) #replace for " width1.csv " for bias estimation when the width is maximus
tm1 <- t(m1)
dim(tm1)
nm <- paste(rep("c",200),seq(1:200), sep = "")
colnames(tm1) <- nm

## Input matrix function

x <- tm1 ## data
ns <- 20 ## samples number
it <- 10 ## iteractions

srm <- function(x, ns, it) {
  row <- length(x[,1])     
  output = matrix(0, row, it)
  for(i in 1:it){
    r = x[,sample(ncol(x), size = ns), drop = FALSE] 
    sr <- apply(r, 1, sum)
    output[,i] <- sr
    ms <- it+1
  }
  output
}

## qD estimation function 

Fp <- srm(tm1, ns, it) ## A name for previous function output

edbu <- function(Fp, it){ OUTPUT <- list()
for(j in 1:1){
  OUTPUT[[j]] <- matrix(0, it, 3)
  colnames(OUTPUT[[j]]) <- c("q0", "q1", "q2")
  rownames(OUTPUT[[j]]) <- 1:it
  for(i in 1:it){
    st1<-t(Fp)
    VE = st1[i,]
    OUTPUT[[j]][i,1] <- sum(VE>0)
    Pi <- VE/sum(VE)
    Pi <- Pi[which(Pi>0)]
    OUTPUT[[j]][i,2]  <- exp(-sum(Pi*log(Pi)))
    OUTPUT[[j]][i,3]  <- 1/ sum(Pi^2)
    OUTPUT[[j]][i,][is.infinite(OUTPUT[[j]][i,])] <- 0
  }
}

OUTPUT2 <- list()
for(j in 1:1){
  Mean <- colMeans(OUTPUT[[j]])
  DS <- sqrt(diag(var(OUTPUT[[j]])))
  IC95i=Mean-(1.96*(DS/sqrt(it)))
  OUTPUT2[[j]] <- data.frame(Mean=Mean, DS=DS, IC95i=IC95i)
  
}
OUTPUT2 <- structure(OUTPUT2, names=colnames(Fp)[2:(it+1)])
OUTPUT2}

edbu(Fp, it) ## review output

## Matrix generation for different sample sizes at 5000 iterations

s1_5 <- srm(tm1, 5, 5000)
s1_10 <- srm(tm1, 10, 5000)
s1_20 <- srm(tm1, 20, 5000)
s1_40 <- srm(tm1, 40, 5000)
s1_80 <- srm(tm1, 80, 5000)
s1_160 <- srm(tm1, 160, 5000)
s1_200 <- srm(tm1, 200, 5000)

w5 <- data.frame(edbu(s1_5, 5000))
w10 <- data.frame(edbu(s1_10, 5000))
w20 <- data.frame(edbu(s1_20, 5000))
w40 <- data.frame(edbu(s1_40, 5000))
w80 <- data.frame(edbu(s1_80, 5000))
w160 <- data.frame(edbu(s1_160, 5000))
w200 <- data.frame(edbu(s1_200, 5000))

## Sorting information

grap <- rbind(w5,w10,w20,w40,w80,w160,w200)
grap$ns <- c(rep(5,3),rep(10,3),rep(20,3),rep(40,3),rep(80,3),rep(160,3),rep(200,3))
rownames(grap) <- seq(1,21)
grap$order <- rep(c("q0", "q1", "q2"), 7)

grap$range <- grap$Mean-grap$IC95i
grap$bias <- abs(1-(grap$Mean/5.063291)) # replace the quotient by the expected value

shannon <- filter(grap, order=="q1")
shannon$bias <- abs(1-(shannon$Mean/6.410701)) # replace the quotient by the expected value

shannon ## table for 1D

levin <- filter(grap, order=="q2")
le05 <- levin
le05$bias <- abs(1-(le05$Mean/5.063291)) # replace the quotient by the expected value

levin ## table for 2D

table_width <- grap

table_width ### sorted table Mean: estimation mean; DS: Standar deviation; IC95%: confidence interval at 95%; ns: sample size; order: Diversity order; range: Mean - IC95%; bias: estimation bias.


#### 2.2 Renkonen (or overlap percentage), Morisita and Pianka symmetric index bias estimation ####

## Packages

library(spaa)

## Data

m1 <- read.csv("rnk1.csv", header = T, row.names = 1) 
tm1 <- t(m1)
dim(tm1)
nm <- paste(rep("c",200),seq(1:200), sep = "")
colnames(tm1) <- nm

m2 <- read.csv("rnk2.csv", header = T, row.names = 1) # use either "rnk1.csv" or "rnk2.csv", both in m1 and m2 for bias estimation when overlap ~ 1
tm2 <- t(m2)
dim(tm2)
nm <- paste(rep("c",200),seq(1:200), sep = "")
colnames(tm2) <- nm

## Input matrix function

x <- tm1 ## data
ns <- 5 ## sample size
it <- 5  ## iterations number

srm <- function(x, ns, it) {
  row <- length(x[,1])     
  output = matrix(0, row, it)
  for(i in 1:it){
    r = x[,sample(ncol(x), size = ns), drop = FALSE] 
    sr <- apply(r, 1, sum)
    output[,i] <- sr
    ms <- it+1
  }
  output
}

## Overlap estimation function

s1 <- srm(tm1, ns, it) ## name for first matrix
s2 <- srm(tm2, ns, it) ## name for second matrix
it <- 5

## For other overlap measures, replace method = "schoener" by method = "morisita" for Morisita measure, or method = "pianka" for Pianka symmetric index


over=function(s1,s2,it){OUTPUT <- list()
for(j in 1:1){
  OUTPUT[[j]] <- matrix(0, it, 1)
  colnames(OUTPUT[[j]]) <- c("overlap")
  rownames(OUTPUT[[j]]) <- 1:it
  for(i in 1:it){
    st1<-t(s1)
    st2<-t(s2)
    Rm=niche.overlap.pair(st1[i,],st2[i,], method = "schoener") ## change method for other overlap functions
    OUTPUT[[j]][i,] <- Rm
    OUTPUT[[j]][i,][is.nan(OUTPUT[[j]][i,])] <- 0
  }
}
OUT2 <- list()
for(j in 1:1){
  Mean=colMeans(OUTPUT[[j]])
  DS=sqrt(diag(var(OUTPUT[[j]])))
  IC95i=Mean-(1.96*(DS/sqrt(it)))
  OUT2[[j]] <- data.frame(Mean=Mean, DS=DS, IC95i=IC95i)
}
OUT2
}

over(s1,s2,it) ## output inspection 

## Matrix generation for different sample sizes at 5000 iterations

s1_5 <- srm(tm1, 5, 5000)
s1_10 <- srm(tm1, 10, 5000)
s1_20 <- srm(tm1, 20, 5000)
s1_40 <- srm(tm1, 40, 5000)
s1_80 <- srm(tm1, 80, 5000)
s1_160 <- srm(tm1, 160, 5000)
s1_200 <- srm(tm1, 200, 5000)

s2_5 <- srm(tm2, 5, 5000)
s2_10 <- srm(tm2, 10, 5000)
s2_20 <- srm(tm2, 20, 5000)
s2_40 <- srm(tm2, 40, 5000)
s2_80 <- srm(tm2, 80, 5000)
s2_160 <- srm(tm2, 160, 5000)
s2_200 <- srm(tm2, 200, 5000)

## Overlap estimation at given sample size

o5 <- data.frame(over(s1_5,s2_5,5000))
o10 <- data.frame(over(s1_10,s2_10,5000))
o20 <- data.frame(over(s1_20,s2_20,5000))
o40 <- data.frame(over(s1_40,s2_40,5000))
o80 <- data.frame(over(s1_80,s2_80,5000))
o160 <- data.frame(over(s1_160,s2_160,5000))
o200 <- data.frame(over(s1_200,s2_200,5000))

grap <- rbind(o5,o10,o20,o40,o80,o160,o200)
rownames(grap) <- seq(1,7)
grap$ns <- c(5,10,20,40,80,160,200)
grap$range <- grap$Mean-grap$IC95i
grap$bias <- (abs(1-(grap$Mean/0.50669819))) # replace the quotient by the expected value

table_renk <- grap

table_renk ## Table for overlap measure. Mean: mean overlap estimation; DS: Standar deviation; IC95i: Confidence interval 95%; ns: Sample size; range: Mean - IC95i; bias: bias estimation.

#### 2.3 1Db and 2Db bias estimation #####

## Packages 

detach("package:spaa", unload = TRUE) ## if spaa package was previously loaded

library(vegetarian)

## Data 

m1 <- read.csv("Q11.csv", header = T, row.names = 1) 
tm1 <- t(m1)
dim(tm1)
nm <- paste(rep("c",200),seq(1:200), sep = "")
colnames(tm1) <- nm

m2 <- read.csv("Q12.csv", header = T, row.names = 1) # use either "Q11.csv" or "Q12.csv", both in m1 and m2 for bias estimation when overlap ~ 1
tm2 <- t(m2)
dim(tm2)
nm <- paste(rep("c",200),seq(1:200), sep = "")
colnames(tm2) <- nm

## Input matrix function

x <- tm1 # Data
ns <- 5 # Sample size
it <- 5 # iterations number

srm <- function(x, ns, it) {
  row <- length(x[,1])     
  output = matrix(0, row, it)
  for(i in 1:it){
    r = x[,sample(ncol(x), size = ns), drop = FALSE] 
    sr <- apply(r, 1, sum)
    output[,i] <- sr
    ms <- it+1
  }
  output
}

## Overlap estimation function 

s1 <- srm(tm1, ns, it) ## name for first matrix
s2 <- srm(tm2, ns, it) ## name for second matrix
it <- 5
q <- 1 ## change either by order 0, 1 and 2, according to the desired diversity order


over=function(s1,s2,it){OUTPUT <- list()
for(j in 1:1){
  OUTPUT[[j]] <- matrix(0, it, 1)
  colnames(OUTPUT[[j]]) <- c("overlap")
  rownames(OUTPUT[[j]]) <- 1:it
  for(i in 1:it){
    st1<-t(s1)
    st2<-t(s2)
    Rm= 1-(turnover(st1[i,],st2[i,], q=q))
    OUTPUT[[j]][i,] <- Rm
    OUTPUT[[j]][i,][is.nan(OUTPUT[[j]][i,])] <- 0
  }
}
OUT2 <- list()
for(j in 1:1){
  Mean=colMeans(OUTPUT[[j]])
  DS=sqrt(diag(var(OUTPUT[[j]])))
  IC95i=Mean-(1.96*(DS/sqrt(it)))
  OUT2[[j]] <- data.frame(Mean=Mean, DS=DS, IC95i=IC95i)
}
OUT2
}

over(s1,s2,it) # output inspection

## Matrix generation for different sample sizes at 5000 iterations

s1_5 <- srm(tm1, 5, 5000)
s1_10 <- srm(tm1, 10, 5000)
s1_20 <- srm(tm1, 20, 5000)
s1_40 <- srm(tm1, 40, 5000)
s1_80 <- srm(tm1, 80, 5000)
s1_160 <- srm(tm1, 160, 5000)
s1_200 <- srm(tm1, 200, 5000)

s2_5 <- srm(tm2, 5, 5000)
s2_10 <- srm(tm2, 10, 5000)
s2_20 <- srm(tm2, 20, 5000)
s2_40 <- srm(tm2, 40, 5000)
s2_80 <- srm(tm2, 80, 5000)
s2_160 <- srm(tm2, 160, 5000)
s2_200 <- srm(tm2, 200, 5000)


### Overlap estimation at given sample size

o5 <- data.frame(over(s1_5,s2_5,5000))
o10 <- data.frame(over(s1_10,s2_10,5000))
o20 <- data.frame(over(s1_20,s2_20,5000))
o40 <- data.frame(over(s1_40,s2_40,5000))
o80 <- data.frame(over(s1_80,s2_80,5000))
o160 <- data.frame(over(s1_160,s2_160,5000))
o200 <- data.frame(over(s1_200,s2_200,5000))


grap <- rbind(o5,o10,o20,o40,o80,o160,o200)
rownames(grap) <- seq(1,7)
grap$ns <- c(5,10,20,40,80,160,200)
grap$range <- grap$Mean-grap$IC95i
grap$bias <- (abs(1-(grap$Mean/0.5086666))) # replace the quotient by the expected value

table_qDb <- grap

table_qDb ## Table for overlap measure. Mean: mean overlap estimation; DS: Standar deviation; IC95i: Confidence interval 95%; ns: Sample size; range: Mean - IC95i; bias: bias estimation.

#### 2.4 Figures 3 and 5 ####

# The base code for the figures shown in Figures 3 and 5 is shown below and can be modified by replacing: 1. "grap" with the name of the table, 2. "ns" with the name of the sample size, 3. "Mean" with the values of the mean of the estimations and 4. yintercept = "X", X = with the expected value.

## packages

library(ggplot2)

## data: use the table of either 2.1 or 2.2 or 2.3 sections 

ggplot(grap, aes(x=ns, y=Mean)) +
  geom_line() +
  geom_point()+
  geom_errorbar(aes(ymin=Mean-range, ymax=Mean+range), width=.2,
                position=position_dodge(0.05))+
  geom_hline(yintercept = 0.5086666, linetype = "solid", color = "gray80", size = 0.8)+ ylim(0,1)+
  xlab("Sample size")+
  ylab("Mean overlap") +theme_classic() + ggtitle(" ") + 
  theme(panel.background=element_rect(fill='transparent',color='black',size=1), 
        plot.title = element_text(family = "Sans", face = "bold", size = 14, hjust=0.5), 
        legend.text = element_text(family = "Sans", size = 14), 
        axis.text = element_text(family = "Sans", size =10, colour = "black"), 
        axis.title = element_text(family = "Sans", face = "bold", size = 20), 
        legend.title = element_text(family = "Sans", face = "bold", size = 14))

#### 2.5 Figures 4 and 6 ####

# The base code for the figures shown in Figures 4 and 6 is shown below and can be modified by replacing: 1. "grap" with the name of the table, 2. "ns" with the name of the sample size and 3. "bias" with the values of the bias of estimation

library(ggplot2)

## data: use the table of either 2.1 or 2.2 or 2.3 sections 

ggplot(grap, aes(x=ns, y=bias)) +
  geom_line() +
  ylim(-0.05, 1.0)+
  geom_point()+
  geom_hline(yintercept = 0.0, linetype = "solid", color = "gray80", size = 0.8)+
  xlab("Sample size")+
  ylab("Bias")+
  theme_classic() + ggtitle(" ") + 
  theme(panel.background=element_rect(fill='transparent',color='black',size=1), 
        plot.title = element_text(family = "Sans", face = "bold", size = 14, hjust=0.5), 
        legend.text = element_text(family = "Sans", size = 14), 
        axis.text = element_text(family = "Sans", size =10, colour = "black"), 
        axis.title = element_text(family = "Sans", face = "bold", size = 20), 
        legend.title = element_text(family = "Sans", face = "bold", size = 14)) + 
  geom_hline(yintercept = 0.1, linetype = "dotted", color = "darkblue", size = 1)
